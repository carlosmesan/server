# Simple Python API in FLASK

## Preparar el entorno

Inicializamos el virtualenv:
```bash
virtualenv -p /usr/bin/python3 .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

## Ejecutar el servidor

El servidor se puede ejecutar de la siguiente manera:
```bash
python3 src/app.py
```

Y comprobar que funciona correctamente:
```bash
curl http://localhost:8080/
```

## Dockerizacion


